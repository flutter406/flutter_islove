import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';

class FlutterIsLove extends StatelessWidget {
  final double size;

  const FlutterIsLove({Key? key, required this.size}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final screenWidth = MediaQuery.of(context).size.width;
    final screenHeight = MediaQuery.of(context).size.height;

    if (screenWidth > screenHeight) {
      return Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          FlutterLogo(size: size),
          Icon(Icons.favorite, color: Colors.red, size: size),
        ],
      );
    } else {
      return Column(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          FlutterLogo(size: size),
          Icon(Icons.favorite, color: Colors.red, size: size),
        ],
      );
    }
  }
}
